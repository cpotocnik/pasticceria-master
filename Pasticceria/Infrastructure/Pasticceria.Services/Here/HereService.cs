﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NFCConnect.Model.CustomSettings;
using NFCConnect.Model.GeoLocalizeHereViewModel;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace NFCConnect.Services.Here
{
    public class HereService : IHereService
    {
        private readonly IOptions<HereSettings> _lSettings;
        private readonly HttpClient _httpClient;

        public HereService(IOptions<HereSettings> lSettings, HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _lSettings = lSettings ?? throw new ArgumentNullException(nameof(lSettings));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public async Task<GeoLocalizeHereViewModel> GeoLocalize(decimal latitude, decimal longitude)
        {
            try
            {
                var uri = _lSettings.Value.ReverseUri;
                var version = _lSettings.Value.Version;
                var appId = _lSettings.Value.AppId;
                var appCode = _lSettings.Value.AppCode;
                var maxResults = _lSettings.Value.MaxResults;
                var mode = _lSettings.Value.Mode;
                var prox = _lSettings.Value.Prox;
                var strLat = latitude.ToString(CultureInfo.InvariantCulture);
                var strLong = longitude.ToString(CultureInfo.InvariantCulture);
                var query = string.Format(uri, version, appId, appCode, maxResults, mode, strLat, strLong, prox);

                var responseString = await _httpClient.GetStringAsync(query);

                var result = JsonConvert.DeserializeObject<GeoLocalizeHereViewModel>(responseString);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
