﻿using NFCConnect.Model.GeoLocalizeHereViewModel;
using System.Threading.Tasks;

namespace NFCConnect.Services.Here
{

    public interface IHereService
    {
        Task<GeoLocalizeHereViewModel> GeoLocalize(decimal latitude, decimal longitude);
    }
}
