﻿using NFCConnect.Model.Data.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NFCConnect.Services.CustomHttpClient
{
    public interface ICustomHttpClient
    {        
        Task<MediaViewModel> GetMediaData(string mediaId);
        Task<List<CustomViewModel>> GetCustomMediaData(string mediaId);
        Task<HttpResponseMessage> GetHtmlData(string mediaId, Guid id);
    }
}
