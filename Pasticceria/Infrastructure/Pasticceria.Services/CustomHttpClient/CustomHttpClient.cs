﻿using Microsoft.Extensions.Configuration;
using NFCConnect.Extensions;
using NFCConnect.Model.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading.Tasks;

namespace NFCConnect.Services.CustomHttpClient
{
    public class CustomHttpClient: ICustomHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public CustomHttpClient(IConfiguration configuration, HttpClient httpClient) 
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));            
        }

        public async Task<HttpResponseMessage> GetHtmlData(string mediaId, Guid id)
        {
            var currentClient = ClientConfigExtensions.GetCurrentClient(_configuration);
            var rootUrl = currentClient.ValidIssuer.First();
            var apiVersion = currentClient.ApiVersion;
            var uri = $"{rootUrl}api/{apiVersion}/Upload/GetHtml?mediaid={mediaId}&id={id}&apiVersion={apiVersion}";
            var request = CreateRequest(uri);
            var result = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
            return result;
            //using (var contentStream = await result.Content.ReadAsStreamAsync())
            //{
            //    return await JsonSerializer.DeserializeAsync<HttpResponseMessage>(contentStream, DefaultJsonSerializerOptions.Options);
            //}

        }

        public async Task<MediaViewModel> GetMediaData(string mediaId)
        {
            var currentClient = ClientConfigExtensions.GetCurrentClient(_configuration);
            var rootUrl = currentClient.ValidIssuer.First();
            var apiVersion = currentClient.ApiVersion;
            var uri = $"{rootUrl}api/{apiVersion}/Media/GetMediaFromItem?mediaId={mediaId}";

            var request = CreateRequest(uri);
            var result = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);

            using (var contentStream = await result.Content.ReadAsStreamAsync())
            {
                return await JsonSerializer.DeserializeAsync<MediaViewModel>(contentStream, DefaultJsonSerializerOptions.Options);
            }
        }
        public async Task<List<CustomViewModel>> GetCustomMediaData(string mediaId)
        {
            var currentClient = ClientConfigExtensions.GetCurrentClient(_configuration);
            var rootUrl = currentClient.ValidIssuer.First();
            var apiVersion = currentClient.ApiVersion;
            var uri = $"{rootUrl}api/{apiVersion}/Custom/Index?mediaId={mediaId}";

            var request = CreateRequest(uri);
            var result = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);

            using (var contentStream = await result.Content.ReadAsStreamAsync())
            {
                return await JsonSerializer.DeserializeAsync<List<CustomViewModel>>(contentStream, DefaultJsonSerializerOptions.Options);
            }
        }
       

        private static HttpRequestMessage CreateRequest(string uri)
        {
            return new HttpRequestMessage(HttpMethod.Get, uri);
        }
    }
}
