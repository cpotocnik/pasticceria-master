﻿using System.Text.Json;

namespace NFCConnect.Services.CustomHttpClient
{
    public static class DefaultJsonSerializerOptions
    {
        public static JsonSerializerOptions Options => new JsonSerializerOptions { PropertyNameCaseInsensitive = true, WriteIndented = true};
    }
}
