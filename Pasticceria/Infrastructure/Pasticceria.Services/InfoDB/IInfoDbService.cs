﻿using NFCConnect.Model.Data.Models.GeoLocalizeViewModel;
using System.Threading.Tasks;

namespace NFCConnect.Services.InfoDB
{
    public interface IInfoDbService
    {
        Task<InfoDbViewModel> GetIpData(string ip);
    }
}
