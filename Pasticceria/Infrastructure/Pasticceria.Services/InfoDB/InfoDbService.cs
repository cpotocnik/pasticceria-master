﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NFCConnect.Model.Data.Models.CustomSettings;
using NFCConnect.Model.Data.Models.GeoLocalizeViewModel;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NFCConnect.Services.InfoDB
{
    public class InfoDbService : IInfoDbService
    {
        private readonly IOptions<InfoDbSettings> _lSettings;
        private readonly HttpClient _httpClient;

        public InfoDbService(IOptions<InfoDbSettings> lSettings, HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _lSettings = lSettings ?? throw new ArgumentNullException(nameof(lSettings));
        }

        public async Task<InfoDbViewModel> GetIpData(string ip)
        {
            var url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json",
                _lSettings.Value.ApiKey, ip);
            var message = await _httpClient.GetAsync(url);
            var json = await message.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<InfoDbViewModel>(json);
            return model;
        }
    }
}
