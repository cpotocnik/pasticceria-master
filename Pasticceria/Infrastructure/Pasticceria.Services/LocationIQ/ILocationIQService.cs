﻿using NFCConnect.Model.GeoLocalizeViewModel;
using System.Threading.Tasks;

namespace NFCConnect.Services.LocationIQ
{
    public interface ILocationIQService
    {
        Task<GeoLocalizeViewModel> GeoLocalize(decimal latitude, decimal longitude);
    }
}
