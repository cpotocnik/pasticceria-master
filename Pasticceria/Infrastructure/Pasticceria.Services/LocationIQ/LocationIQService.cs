﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NFCConnect.Model.CustomSettings;
using NFCConnect.Model.GeoLocalizeViewModel;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NFCConnect.Services.LocationIQ
{
    public class LocationIQService : ILocationIQService
    {
        private readonly IOptions<LocationIQSettings> _lSettings;
        private readonly HttpClient _httpClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lSettings"></param>
        public LocationIQService(HttpClient httpClient, IOptions<LocationIQSettings> lSettings)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _lSettings = lSettings ?? throw new ArgumentNullException(nameof(lSettings));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public async Task<GeoLocalizeViewModel> GeoLocalize(decimal latitude, decimal longitude)
        {
            try
            {
                var uri = _lSettings.Value.EUReverseUri;
                var token = _lSettings.Value.Token;
                var query = string.Format(uri, token, latitude, longitude);

                var responseString = await _httpClient.GetStringAsync(query);

                var result = JsonConvert.DeserializeObject<GeoLocalizeViewModel>(responseString);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
