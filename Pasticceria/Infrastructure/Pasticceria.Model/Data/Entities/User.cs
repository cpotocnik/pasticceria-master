﻿using Microsoft.AspNetCore.Identity;
using NFCConnect.Model.Data.Attributes;
using System;
namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class User : IdentityUser
    {
        public string Id { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string ImageUrl { get; set; }
        [Encrypted]
        public string UserName { get; set; }
        [Encrypted]
        public string NormalizedUserName { get; set; }
        [Encrypted]
        public string Email { get; set; }
        [Encrypted]
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
    }
}
