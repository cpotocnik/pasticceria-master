﻿using AGJ.Data;
using System;

namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class Client : Entity
    {
        public int Id { get; set; }
        public bool Enabled { get; set; }
        public bool RequireClientSecret { get; set; }
        public string ClientId { get; set; }
        public string ProtocolType { get; set; }
        public string ClientName { get; set; }
        public Nullable<DateTime> Created { get; set; }
    }
}
