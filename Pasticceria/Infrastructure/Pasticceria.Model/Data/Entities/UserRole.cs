﻿using AGJ.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class UserRole : Entity
    {
        [Key]
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}
