﻿using AGJ.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class IdentityResource : Entity
    {
        [Key]
        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Required { get; set; }
        public bool Emphasize { get; set; }
        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Updated { get; set; }
        public bool NonEditable { get; set; }
    }
}

