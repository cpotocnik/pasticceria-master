﻿using AGJ.Data;
using System;

namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class APIResource : Entity
    {
        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Updated { get; set; }
        public Nullable<DateTime> LastAccessed { get; set; }
        public bool NonEditable { get; set; }
    }
}
