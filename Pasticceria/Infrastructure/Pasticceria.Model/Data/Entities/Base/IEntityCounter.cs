﻿using AGJ.Data;
using Newtonsoft.Json;
using System;

namespace NFCConnect.Model.Data.Entities.Base
{
    public interface IEntityCounter : IEntityValid
    {
        string Name { get; set; }
        long NTaps { get; set; }
        long NPlay { get; set; }
        long NCustom { get; set; }
        long NViewTime { get; set; }
        long NItems { get; set; }
    }

    [Serializable]
    public abstract class EntityCounter : EntityValid, IEntityCounter
    {
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        public long NTaps { get; set; }
        public long NPlay { get; set; }
        public long NCustom { get; set; }
        public long NViewTime { get; set; }
        public long NItems { get; set; }

        protected EntityCounter()
        {
            NTaps = 0;
            NPlay = 0;
            NCustom = 0;
            NViewTime = 0;
            NItems = 0;
        }
    }
}