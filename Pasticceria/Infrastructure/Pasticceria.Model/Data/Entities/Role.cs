﻿using NFCConnect.Model.Data.Entities.Base;
using System;

namespace NFCConnect.Model.Data.Entities
{
    [Serializable]
    public class Role : EntityCounter
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
