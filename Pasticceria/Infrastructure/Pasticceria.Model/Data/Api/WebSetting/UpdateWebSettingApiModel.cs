﻿namespace NFCConnect.Model.Data.Api.WebSetting
{
    public class UpdateWebSettingApiModel
    {
        public string Language { get; set; }
        public string HeaderColor { get; set; }
        public string HeaderItemsColor { get; set; }
        public string DashboardColor { get; set; }
        public bool CustomTheme { get; set; }
    }
}
