﻿using Newtonsoft.Json;

namespace NFCConnect.Model.Data.Api.Jira
{
    public class SupportRequestModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("priority")]
        public int Priority { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
