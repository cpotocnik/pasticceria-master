﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace NFCConnect.Model.Data.Api.Jira
{
    public partial class IssueApiModel
    {
        [JsonProperty("update")]
        public Update Update { get; set; }

        [JsonProperty("fields")]
        public Fields Fields { get; set; }
    }

    public partial class Fields
    {
        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("issuetype")]
        public Issuetype Issuetype { get; set; }

        [JsonProperty("project")]
        public Issuetype Project { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("reporter")]
        public Reporter Reporter { get; set; }

        [JsonProperty("fixVersions")]
        public object[] FixVersions { get; set; }

        [JsonProperty("priority")]
        public Priority Priority { get; set; }

        [JsonProperty("labels")]
        public object[] Labels { get; set; }

        [JsonProperty("assignee")]
        public object Assignee { get; set; }
    }

    public partial class Description
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("version")]
        public long Version { get; set; }

        [JsonProperty("content")]
        public DescriptionContent[] Content { get; set; }
    }

    public partial class DescriptionContent
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("content")]
        public ContentContent[] Content { get; set; }
    }

    public partial class ContentContent
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class Issuetype
    {
        [JsonProperty("_comment")]
        public string Comment { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class Priority
    {
        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }
    }

    public partial class Reporter
    {
        [JsonProperty("accountId")]
        public string AccountId { get; set; }
    }

    public partial class Update
    {
    }

    public partial class Issue
    {
        public static Issue FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Issue>(json, Converter.Settings);
        }
    }

    public static class Serialize
    {
        public static string ToJson(this Issue self)
        {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t)
        {
            return t == typeof(long) || t == typeof(long?);
        }

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            var value = serializer.Deserialize<string>(reader);
            if (long.TryParse(value, out var l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}

