﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Api.Setup
{
    [Serializable]
    public class UpdateTranslationsApiModel
    {     
        public string Language { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
