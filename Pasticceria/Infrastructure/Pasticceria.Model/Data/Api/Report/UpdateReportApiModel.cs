﻿using NFCConnect.Model.Data.Entities;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Api.Report
{
    public class UpdateReportApiModel
    {
        public Guid ProductId { get; set; }
        public string Url { get; set; }
        public ReportFrequency ReportFrequency { get; set; }
        public List<MailApiModel> Mails { get; set; }
    }

    public class MailApiModel
    {
        public string Email { get; set; }
    }
}
