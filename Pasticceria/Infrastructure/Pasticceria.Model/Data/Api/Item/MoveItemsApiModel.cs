﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Item
{
    public class MoveItemsApiModel
    {
        [Required]
        public Guid DestCategoryId { get; set; }
        [Required]
        public List<string> MediaIds { get; set; }
    }
}
