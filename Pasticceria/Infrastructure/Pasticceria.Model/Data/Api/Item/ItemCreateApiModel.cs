﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Item
{
    public class ItemCreateApiModel
    {
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        public int Quantity { get; set; }     
        public bool BookEnableExpiration { get; set; }
        public int BookExpirationDays { get; set; }
    }
}