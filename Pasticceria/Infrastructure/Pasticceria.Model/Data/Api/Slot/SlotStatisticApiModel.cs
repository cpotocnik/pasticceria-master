﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Api.Slot
{
    public class SlotStatisticApiModel
    {
        public int NViewTime { get; set; }
        public Guid SlotStatisticId { get; set; }
    }
}
