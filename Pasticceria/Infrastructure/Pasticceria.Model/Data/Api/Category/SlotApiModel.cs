﻿using NFCConnect.Model.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Category
{
    public class SlotApiModel
    {
        [Required]
        public Guid Id { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
        public SlotType SlotType { get; set; }
        public string Url { get; set; }
        public string ThumbUrl { get; set; }
        public int MaxVideoLenght { get; set; }
        public Guid CategoryId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }
}
