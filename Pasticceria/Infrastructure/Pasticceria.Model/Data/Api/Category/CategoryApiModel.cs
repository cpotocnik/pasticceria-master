﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Category
{
    public class CategoryApiModel
    {
        [Required]
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public Guid ProductId { get; set; }
        public Guid? ParentId { get; set; }
        public string ImageUrl { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Plot { get; set; }
        public string Author { get; set; }
        public string Speaker { get; set; }
        public int FadingTimeout { get; set; }
    }
}
