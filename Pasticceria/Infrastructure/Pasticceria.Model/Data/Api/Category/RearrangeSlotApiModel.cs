﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Category
{
    public class RearrangeSlotApiModel
    {
        [Required]
        public Guid SlotId { get; set; }
        [Required]
        public int Index { get; set; }
    }
}
