﻿using System;

namespace NFCConnect.Model.Data.Api.News
{
    public class NewsApiModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime StartsAt { get; set; }
        public DateTime EndsAt { get; set; }
    }
}
