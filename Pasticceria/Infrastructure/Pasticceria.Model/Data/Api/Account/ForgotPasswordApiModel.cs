﻿using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Account
{
    public class ForgotPasswordApiModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
