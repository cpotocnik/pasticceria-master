﻿using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Account
{
    public class LoginWithRecoveryCodeApiModel
    {
        [Required]
        public string RecoveryCode { get; set; }
    }
}
