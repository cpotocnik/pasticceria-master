﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Api.Account
{
    [Serializable]
    public class LoginApiModel
    {
        [Required]
        [EmailAddress]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
