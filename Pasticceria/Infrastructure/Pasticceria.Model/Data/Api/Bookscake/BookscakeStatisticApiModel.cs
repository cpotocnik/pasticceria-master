﻿using System;

namespace NFCConnect.Model.Data.Api.Bookscake
{
    public class BookscakeStatisticApiModel
    {
        public string Uid { get; set; }
        public string Os { get; set; }
        public string Language { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string MediaId { get; set; }
        public int Chapter { get; set; }
        public int NListenTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool Geolocalized { get; set; }
    }
}
