﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Api.Bookscake
{
    public class DownloadReportApiModel
    {
        public List<Guid> Ids { get; set; }
    }
}
