﻿namespace NFCConnect.Model.Data.Enums
{
    public enum FolderType
    {
        Fixed,
        Custom,
        Cover,
        Avatar,
        Shared,
        Thumb,
        Pdf
    }
}
