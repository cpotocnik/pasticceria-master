﻿namespace NFCConnect.Model.Data.Enums
{
    public enum MediaType
    {
        Image,
        Video
    }
}
