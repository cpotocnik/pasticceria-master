﻿namespace NFCConnect.Model.Data.Enums
{
    public enum ComponentName
    {
        Activation,
        Cdn77,
        Encryption,
        Report,
        Swagger
    }
}
