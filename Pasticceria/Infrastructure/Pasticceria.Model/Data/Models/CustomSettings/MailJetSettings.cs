﻿namespace NFCConnect.Model.CustomSettings
{
    public class MailJetSettings
    {
        /// <summary>
        /// MJHost
        /// </summary>
        public string MJHost { get; set; }
        /// <summary>
        /// MJPort
        /// </summary>
        public string MJPort { get; set; }
        /// <summary>
        /// MJUseTLS
        /// </summary>
        public bool MJUseTLS { get; set; }
        /// <summary>
        /// MJPublicKey
        /// </summary>
        public string MJPublicKey { get; set; }
        /// <summary>
        /// MJPrivateKey
        /// </summary>
        public string MJPrivateKey { get; set; }
        /// <summary>
        /// Version
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// SendMail
        /// </summary>
        public string SendMail { get; set; }
        /// <summary>
        /// MJTemplateID
        /// </summary>
        public int MJTemplateID { get; set; }
        /// <summary>
        /// MJTemplateLanguage
        /// </summary>
        public bool MJTemplateLanguage { get; set; }
        /// <summary>
        /// ReportEmail
        /// </summary>
        public string ReportEmail { get; set; }
    }
}
