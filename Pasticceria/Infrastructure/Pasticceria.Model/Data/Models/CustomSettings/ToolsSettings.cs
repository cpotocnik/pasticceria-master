﻿namespace NFCConnect.Model.CustomSettings
{
    public class ToolsSettings
    {
        /// <summary>
        /// HtmlFileName
        /// </summary>
        public string HtmlFileName { get; set; }
        /// <summary>
        /// RedirectFileName
        /// </summary>
        public string RedirectFileName { get; set; }
    }
}
