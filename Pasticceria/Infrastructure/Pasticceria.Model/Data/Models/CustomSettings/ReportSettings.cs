﻿namespace NFCConnect.Model.CustomSettings
{
    public class ReportSettings
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public string FromAddress { get; set; }
        public string FileExtension { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string DashboardUrl { get; set; }
    }
}
