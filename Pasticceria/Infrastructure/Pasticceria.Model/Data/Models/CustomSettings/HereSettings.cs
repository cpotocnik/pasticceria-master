﻿namespace NFCConnect.Model.CustomSettings
{
    public class HereSettings
    {
        public string ReverseUri { get; set; }
        public string Version { get; set; }
        public string AppId { get; set; }
        public string AppCode { get; set; }
        public int MaxResults { get; set; }
        public string Mode { get; set; }
        public string Prox { get; set; }
    }
}
