﻿namespace NFCConnect.Model.CustomSettings
{
    public class JiraAccountSettings
    {
        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// Endpoint
        /// </summary>
        public string Endpoint { get; set; }
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// ApiToken
        /// </summary>
        public string ApiToken { get; set; }
        /// <summary>
        /// ProjectId
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// IssueType
        /// </summary>
        public string IssueType { get; set; }
        /// <summary>
        /// Summary
        /// </summary>
        public string Summary { get; set; }
        /// <summary>
        /// ReporterID
        /// </summary>
        public string ReporterID { get; set; }
        /// <summary>
        /// Priority
        /// </summary>
        public long Priority { get; set; }
    }
}
