﻿namespace NFCConnect.Model.CustomSettings
{
    public class SetupSettings
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Pass
        /// </summary>
        public string Pass { get; set; }
        /// <summary>
        /// Roles
        /// </summary>
        public string[] Roles { get; set; }
        /// <summary>
        /// VideoFormat
        /// </summary>
        public string VideoFormat { get; set; }
        /// <summary>
        /// VideoFormat
        /// </summary>
        public string ImageFormat { get; set; }
        /// <summary>
        /// AuthUriFormat
        /// </summary>
        public string AuthUriFormat { get; set; }
        /// <summary>
        /// User Roles
        /// </summary>
        public string[] UserRoles { get; set; }
        /// <summary>
        /// BooksCake Role
        /// </summary>
        public string BooksCakeRole { get; set; }
        /// <summary>
        /// Admin Role
        /// </summary>
        public string AdminRole { get; set; }
        /// <summary>
        /// Max Media Size
        /// </summary>
        public long MaxMediaSize { get; set; }
    }
}
