﻿namespace NFCConnect.Model.CustomSettings
{
    public class LocationIQSettings
    {
        public string USReverseUri { get; set; }
        public string EUReverseUri { get; set; }
        public string Token { get; set; }
    }
}
