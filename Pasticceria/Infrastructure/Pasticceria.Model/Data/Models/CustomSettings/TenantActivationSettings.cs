﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.CustomSettings
{
    public class TenantActivationSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public string Duration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NotificationEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NotificationText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NotificationSubject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MJTemplateID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MJTemplateLanguage { get; set; }
    }
}
