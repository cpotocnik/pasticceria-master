﻿namespace NFCConnect.Model.Data.Models.CustomSettings
{
    public class CDNSettings
    {
        /// <summary>
        /// Protocol
        /// </summary>
        public string Protocol { get; set; }
        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// User
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Uri
        /// </summary>
        public string Uri { get; set; }
        /// <summary>
        /// SecureToken
        /// </summary>
        public string SecureToken { get; set; }
        /// <summary>
        /// EnableSecureLink
        /// </summary>
        public bool EnableSecureLink { get; set; }
        /// <summary>
        /// ExpiringMinutes
        /// </summary>
        public long ExpiringMinutes { get; set; }
    }
}
