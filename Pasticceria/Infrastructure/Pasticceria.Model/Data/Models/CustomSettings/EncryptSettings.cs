﻿namespace NFCConnect.Model.CustomSettings
{
    public class EncryptSettings
    {
        /// <summary>
        /// Key
        /// </summary>
        public string Key { get; set; }
    }
}
