﻿namespace NFCConnect.Model.CustomSettings
{
    public class MailSettings
    {
        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// Port
        /// </summary>
        public string Port { get; set; }
        /// <summary>
        /// User
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// EnableSsl
        /// </summary>
        public bool EnableSsl { get; set; }
    }
}
