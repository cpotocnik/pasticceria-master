﻿using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.CustomSettings
{
    public class AccountReportSettings
    {
        [EmailAddress]
        public string Username { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string ChromeDriverVersion { get; set; }
    }
}
