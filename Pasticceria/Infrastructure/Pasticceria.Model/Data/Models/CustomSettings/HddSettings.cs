﻿namespace NFCConnect.Model.Data.Models.CustomSettings
{
    public class HddSettings
    {
        public string BucketName { get; set; }
        public string LocalPathMedia { get; set; }
        public string UrlMedia { get; set; }
    }
}
