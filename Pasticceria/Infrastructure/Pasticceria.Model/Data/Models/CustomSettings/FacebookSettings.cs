﻿namespace NFCConnect.Model.CustomSettings
{
    public class FacebookSettings
    {
        /// <summary>
        /// Id of the app
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// The app secret
        /// </summary>
        public string AppSecret { get; set; }
        /// <summary>
        /// The app fields
        /// </summary>
        public string Fields { get; set; }
        /// <summary>
        /// The app uri
        /// </summary>
        public string BaseAddress { get; set; }

        /// <summary>
        /// The test video description
        /// </summary>
        public string TestVideoDescription { get; set; }
        /// <summary>
        /// The test video title
        /// </summary>
        public string TestVideoTitle { get; set; }
        /// <summary>
        /// The test video uri
        /// </summary>
        public string TestVideoUri { get; set; }

    }
}
