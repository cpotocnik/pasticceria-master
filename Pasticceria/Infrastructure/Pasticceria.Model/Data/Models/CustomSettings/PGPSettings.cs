﻿namespace NFCConnect.Model.Data.Models.CustomSettings
{
    public class PGPSettings
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Path
        /// </summary>
        public string Path { get; set; }
    }
}
