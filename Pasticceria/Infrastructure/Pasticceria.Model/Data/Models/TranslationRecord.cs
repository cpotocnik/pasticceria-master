﻿namespace NFCConnect.Model.Data.Models
{
    public class TranslationRecord
    {
        public string Language { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
