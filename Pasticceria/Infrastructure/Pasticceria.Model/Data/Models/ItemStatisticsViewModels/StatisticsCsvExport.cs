﻿using AGJ.Data.Repository.Csv.Attributes;
using AGJ.Domain;

namespace NFCConnect.Models.ExportViewModels
{
    [CsvFile(";", true)]
    public class StatisticsCsvExport : AgjFileCsv
    {
        [CsvOutputField(1, "MediaId")]
        public string MediaId { get; set; }
        [CsvOutputField(2, "CustomerId")]
        public string CustomerId { get; set; }
        [CsvOutputField(3, "CategoryName")]
        public string CategoryName { get; set; }
        [CsvOutputField(4, "Ip")]
        public string Ip { get; set; }
        [CsvOutputField(5, "Region")]
        public string Region { get; set; }
        [CsvOutputField(6, "City")]
        public string City { get; set; }
        [CsvOutputField(7, "Country")]
        public string Country { get; set; }
        [CsvOutputField(8, "Os")]
        public string Os { get; set; }
        [CsvOutputField(9, "Language")]
        public string Language { get; set; }
        [CsvOutputField(10, "LocalDateTime")]
        public string LocalDateTime { get; set; }
        [CsvOutputField(11, "Longitude")]
        public string Longitude { get; set; }
        [CsvOutputField(12, "Latitude")]
        public string Latitude { get; set; }
        //[CsvOutputField(12, "PostalCode")]
        //public string PostalCode { get; set; }
        //[CsvOutputField(13, "Organisation")]
        //public string Organisation { get; set; }
    }
}
