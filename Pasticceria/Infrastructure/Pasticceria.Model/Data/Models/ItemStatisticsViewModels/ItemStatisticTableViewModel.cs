﻿using NFCConnect.Models.ItemStatisticsViewModels;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ItemStatisticsViewModels
{
    public class ItemStatisticTableViewModel
    {
        public int RecordsTotal { get; set; }
        public List<ItemStatisticRowViewModel> ItemStatisticRowViewModels { get; set; }
    }
}
