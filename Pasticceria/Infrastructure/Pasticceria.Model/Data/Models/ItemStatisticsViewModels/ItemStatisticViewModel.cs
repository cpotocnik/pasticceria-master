﻿using NFCConnect.Model.Data.Models.GeoLocalizeViewModel;
using System;

namespace NFCConnect.Models
{
    public class Asn
    {
        public string asn { get; set; }
        public string name { get; set; }
        public string domain { get; set; }
        public string route { get; set; }
        public string type { get; set; }
    }

    public class Company
    {
        public string name { get; set; }
        public string domain { get; set; }
        public string type { get; set; }
    }

    public class IpInfoViewModel
    {
        public string ip { get; set; }
        public string hostname { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string loc { get; set; }
        public Asn asn { get; set; }
        public Company company { get; set; }
    }

    public class ItemStatisticViewModel
    {
        public IpInfoViewModel IpInfo { get; set; }
        public string Uid { get; set; }
        public string Os { get; set; }
        public string Language { get; set; }
        public string LocalDate { get; set; }
        public string MediaId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IsGeolocalized { get; set; }
        public Guid StatisticId { get; set; }
    }

    public class StatisticViewModel
    {
        public InfoDbViewModel InfoDb { get; set; }
        public string Uid { get; set; }
        public string Os { get; set; }
        public string Language { get; set; }
        public string LocalDate { get; set; }
        public string MediaId { get; set; }
    }

    public class UpdateStatisticViewModel
    {
        public Guid id { get; set; }
        public int time { get; set; }
        public bool redirected { get; set; }
    }

    public class UpdateGeolocalizationViewModel
    {
        public Guid id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}