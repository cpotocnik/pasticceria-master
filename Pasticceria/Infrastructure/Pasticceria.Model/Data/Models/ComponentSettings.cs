﻿using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{

    public class ComponentSettings
    {
        public List<Component> Components { get; set; }
    }

    public class Component
    {
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }
}
