﻿using Newtonsoft.Json;
namespace NFCConnect.Model.GeoLocalizeHereViewModel
{


    public partial class GeoLocalizeHereViewModel
    {
        [JsonProperty("Response")]
        public Response Response { get; set; }
    }

    public partial class Response
    {
        [JsonProperty("MetaInfo")]
        public MetaInfo MetaInfo { get; set; }

        [JsonProperty("View")]
        public View[] View { get; set; }
    }

    public partial class MetaInfo
    {
        [JsonProperty("Timestamp")]
        public string Timestamp { get; set; }
    }

    public partial class View
    {
        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("ViewId")]
        public long ViewId { get; set; }

        [JsonProperty("Result")]
        public Result[] Result { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("Relevance")]
        public long Relevance { get; set; }

        [JsonProperty("Distance")]
        public long Distance { get; set; }

        [JsonProperty("Direction")]
        public long Direction { get; set; }

        [JsonProperty("MatchLevel")]
        public string MatchLevel { get; set; }

        [JsonProperty("MatchQuality")]
        public MatchQuality MatchQuality { get; set; }

        [JsonProperty("Location")]
        public Location Location { get; set; }
    }

    public partial class Location
    {
        [JsonProperty("LocationId")]
        public string LocationId { get; set; }

        [JsonProperty("LocationType")]
        public string LocationType { get; set; }

        [JsonProperty("DisplayPosition")]
        public DisplayPosition DisplayPosition { get; set; }

        [JsonProperty("MapView")]
        public MapView MapView { get; set; }

        [JsonProperty("Address")]
        public Address Address { get; set; }

        [JsonProperty("MapReference")]
        public MapReference MapReference { get; set; }
    }

    public partial class Address
    {
        [JsonProperty("Label")]
        public string Label { get; set; }

        [JsonProperty("Country")]
        public string Country { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("County")]
        public string County { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("District")]
        public string District { get; set; }

        [JsonProperty("PostalCode")]
        public long PostalCode { get; set; }

        [JsonProperty("AdditionalData")]
        public AdditionalDatum[] AdditionalData { get; set; }
    }

    public partial class AdditionalDatum
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public partial class DisplayPosition
    {
        [JsonProperty("Latitude")]
        public double Latitude { get; set; }

        [JsonProperty("Longitude")]
        public double Longitude { get; set; }
    }

    public partial class MapReference
    {
        [JsonProperty("ReferenceId")]
        public long ReferenceId { get; set; }

        [JsonProperty("SideOfStreet")]
        public string SideOfStreet { get; set; }

        [JsonProperty("CountryId")]
        public long CountryId { get; set; }

        [JsonProperty("StateId")]
        public long StateId { get; set; }

        [JsonProperty("CountyId")]
        public long CountyId { get; set; }

        [JsonProperty("CityId")]
        public long CityId { get; set; }

        [JsonProperty("DistrictId")]
        public long DistrictId { get; set; }
    }

    public partial class MapView
    {
        [JsonProperty("TopLeft")]
        public DisplayPosition TopLeft { get; set; }

        [JsonProperty("BottomRight")]
        public DisplayPosition BottomRight { get; set; }
    }

    public partial class MatchQuality
    {
        [JsonProperty("Country")]
        public long Country { get; set; }

        [JsonProperty("State")]
        public long State { get; set; }

        [JsonProperty("County")]
        public long County { get; set; }

        [JsonProperty("City")]
        public long City { get; set; }

        [JsonProperty("District")]
        public long District { get; set; }

        [JsonProperty("PostalCode")]
        public long PostalCode { get; set; }
    }

}
