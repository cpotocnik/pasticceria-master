﻿namespace NFCConnect.Model.Data.Models.GeoLocalizeViewModel
{
    public class InfoDbViewModel
    {
        public string IpAddress { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string CityName { get; set; }
        public string RegionName { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
    }
}
