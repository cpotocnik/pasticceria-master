﻿using Newtonsoft.Json;
using System;

namespace NFCConnect.Model.GeoLocalizeViewModel
{
    public partial class GeoLocalizeViewModel
    {
        [JsonProperty("place_id")]
        public long PlaceId { get; set; }

        [JsonProperty("licence")]
        public Uri Licence { get; set; }

        [JsonProperty("osm_type")]
        public string OsmType { get; set; }

        [JsonProperty("osm_id")]
        public long OsmId { get; set; }

        [JsonProperty("lat")]
        public string Lat { get; set; }

        [JsonProperty("lon")]
        public string Lon { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("boundingbox")]
        public string[] Boundingbox { get; set; }
    }

    public partial class Address
    {
        [JsonProperty("address29")]
        public string Address29 { get; set; }

        [JsonProperty("footway")]
        public string Footway { get; set; }

        [JsonProperty("suburb")]
        public string Suburb { get; set; }

        [JsonProperty("city_district")]
        public string CityDistrict { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("postcode")]
        public long Postcode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
    }
}
