﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{
    public class ClientSettings : Client
    {
        public ICollection<string> ClientRoles { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public bool IncludeErrorDetails { get; set; }
        public string SecretKey { get; set; }
        public bool ValidateIssuer { get; set; }
        public bool ValidateAudience { get; set; }
        public bool ValidateLifetime { get; set; }
        public bool ValidateIssuerSigningKey { get; set; }
        public ICollection<string> ValidIssuer { get; set; }
        public ICollection<string> ValidAudience { get; set; }
        public string IssuerSigningKey { get; set; }
        public string ApiVersion { get; set; }
    }
}
