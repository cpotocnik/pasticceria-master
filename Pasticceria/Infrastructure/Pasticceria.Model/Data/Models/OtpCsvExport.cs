﻿using AGJ.Data.Repository.Csv.Attributes;
using AGJ.Domain;
using System;

namespace NFCConnect.Models
{
    [CsvFile(";", true)]
    public class OtpCsvExport : AgjFileCsv
    {
        [CsvOutputField(1, "Otp")]
        public string Otp { get; set; }
        [CsvOutputField(2, "CreationTime")]
        public DateTime CreationTime { get; set; }
        [CsvOutputField(3, "Status")]
        public bool Status { get; set; }
    }
}