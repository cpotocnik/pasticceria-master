﻿using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{
    public class ClientsConfiguration
    {
        public ICollection<ClientSettings> ClientSettings { get; set; }
    }
}
