﻿namespace NFCConnect.Model.Data.Models.NxpSettings
{
    public class NxpSettings
    {
        public string OriginKey { get; set; }
        public string CmacKey { get; set; }
        public string PiccKey { get; set; }
    }
}
