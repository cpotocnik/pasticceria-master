﻿namespace NFCConnect.Model.HostSettings
{
    public class HostSettings
    {
        /// <summary>
        /// Host client name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Active GeoCoding Service
        /// </summary>
        public string GeoCodingService { get; set; }
    }
}
