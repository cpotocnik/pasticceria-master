﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models
{
    public class OtpSearch
    {
        public string Otp { get; set; }
        public string Isbn { get; set; }
        public string Ean13 { get; set; }
        public string Pos { get; set; }
    }
}
