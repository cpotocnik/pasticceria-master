﻿using Microsoft.AspNetCore.Identity;
using NFCConnect.Model.Data.Attributes;
using NFCConnect.Model.Data.Entities;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            UserDevices = new List<UserDevice>();
        }

        public DateTime? LastLoginTime { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public int WrongTries { get; set; }
        public DateTime? LastWrongTry { get; set; }
        public string ImageUrl { get; set; }
        [Encrypted]
        public override string UserName { get; set; }
        [Encrypted]
        public override string NormalizedUserName { get; set; }
        [Encrypted]
        public override string Email { get; set; }
        [Encrypted]
        public override string NormalizedEmail { get; set; }

        public virtual ICollection<UserDevice> UserDevices { get; set; }
    }
}
