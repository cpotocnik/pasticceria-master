﻿using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
