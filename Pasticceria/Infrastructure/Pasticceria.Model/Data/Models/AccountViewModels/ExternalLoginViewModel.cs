using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
