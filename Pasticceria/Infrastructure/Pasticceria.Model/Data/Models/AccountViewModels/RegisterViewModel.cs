﻿using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Old password")]
        public string oldPassword { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "New Roles")]
        public string[] Roles { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "New Roles")]
        public string newRole { get; set; }
    }
}
