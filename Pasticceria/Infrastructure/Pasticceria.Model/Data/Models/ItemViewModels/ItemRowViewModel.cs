﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace NFCConnect.Model.Data.Models.ItemViewModels
{
    [Serializable]
    public class ItemRowViewModel
    {
        public bool Check { get; set; }
        [Key]
        [JsonProperty(PropertyName = "MediaId")]
        public string MediaId { get; set; }
        [JsonProperty(PropertyName = "CustomerId")]
        public string CustomerId { get; set; }
        [JsonProperty(PropertyName = "CategoryName")]
        public string CategoryName { get; set; }
        [JsonProperty(PropertyName = "NTaps")]
        public long NTaps { get; set; }
        [JsonProperty(PropertyName = "NPlay")]
        public long NPlay { get; set; }
        [JsonProperty(PropertyName = "NViewTime")]
        public long NViewTime { get; set; }
        [JsonProperty(PropertyName = "NMaxTaps")]
        public int NMaxTaps { get; set; }
        // FR: Proprietà aggiunte su richiesta di Maurizio Galli per visualizzazione su F.E.
        [JsonProperty(PropertyName = "Region")]
        public string Region { get; set; }
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "DateFrom")]
        public DateTime DateFrom { get; set; }
        [JsonProperty(PropertyName = "DateTo")]
        public DateTime DateTo { get; set; }
        // FR: Proprietà aggiunte per gestire book expiration
        [JsonProperty(PropertyName = "EnableExpiration")]
        public bool BookEnableExpiration { get; set; }
        [JsonProperty(PropertyName = "ExpirationDays")]
        public int BookExpirationDays { get; set; }
        [JsonProperty(PropertyName = "UnlockDate")]
        public string BookUnlockDate { get; set; }
    }
}