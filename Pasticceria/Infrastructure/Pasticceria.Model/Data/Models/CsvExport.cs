﻿using AGJ.Data.Repository.Csv.Attributes;
using AGJ.Domain;

namespace NFCConnect.Models
{
    [CsvFile(";", true)]
    public class CsvExport : AgjFileCsv
    {
        [CsvOutputField(1, "MediaId")]
        public string MediaId { get; set; }
        [CsvOutputField(2, "CustomerId")]
        public string CustomerId { get; set; }
        [CsvOutputField(3, "CategoryName")]
        public string CategoryName { get; set; }
        [CsvOutputField(4, "NTaps")]
        public long NTaps { get; set; }
        [CsvOutputField(5, "NPlay")]
        public long NPlay { get; set; }
        [CsvOutputField(6, "NViewTime")]
        public long NViewTime { get; set; }
        [CsvOutputField(7, "EnableExpiration")]
        public bool BookEnableExpiration { get; set; }
        [CsvOutputField(8, "ExpirationDays")]
        public int BookExpirationDays { get; set; }
    }
}