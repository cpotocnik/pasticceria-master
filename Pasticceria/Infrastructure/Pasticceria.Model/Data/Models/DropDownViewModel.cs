﻿using System;

namespace NFCConnect.Model.Data.Models
{
    public class DropDownViewModel
    {
        public Guid Value { get; set; }
        public string Name { get; set; }
    }
}
