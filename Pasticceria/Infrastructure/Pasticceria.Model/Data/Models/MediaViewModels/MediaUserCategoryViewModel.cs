﻿using NFCConnect.Model.Data.Enums;
using NFCConnect.Model.Data.Models.ApiViewModels.Bookscake;

namespace NFCConnect.Model.Data.Models.MediaViewModels
{
    public class MediaUserCategoryViewModel : ApiUserCategoryViewModel
    {
        /// <summary>
        /// Media url
        /// </summary>
        public string UrlMedia { get; set; }

        public MediaType Type { get; set; }

        public string MediaName { get; set; }
    }
}
