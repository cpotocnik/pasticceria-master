﻿using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{
    public class ApplicationUserRoles : ApplicationUser
    {
        public IList<string> Roles { get; set; }
    }
}
