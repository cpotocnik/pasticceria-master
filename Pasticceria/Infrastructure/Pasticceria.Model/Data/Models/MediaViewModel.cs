﻿using Newtonsoft.Json;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Model.Data.Models.ApiViewModels.Category;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models
{
    public class MediaViewModel
    {
        public string MediaId { get; set; }
        public string ResponseUrl { get; set; }
        public string ResponseUrlApi { get; set; }
        public string ApiVersion { get; set; }
        public bool HasVideoLoaded { get; set; }
        public bool HasVideoCustomizable { get; set; }
        public bool HasVideoCustomLoaded { get; set; }
        public bool HasRedirect { get; set; }
        public string Redirect { get; set; }
        public string CheckCode { get; set; }
        public SlotRedirectViewModel SlotRedirectViewModel { get; set; }
        public SlotCustomViewModel SlotCustomViewModel { get; set; }
        public List<SlotFixedViewModel> SlotFixedViewModels { get; set; }
        public Guid ItemStatisticId { get; set; }
        public int FadingTimeOut { get; set; }
    }

    public class CustomViewModel
    {
        public string MediaId { get; set; }
        public Guid ContentId { get; set; }
        public string ResponseUrl { get; set; }
        public string CheckCode { get; set; }
    }

    public class SlotRedirectViewModel
    {
        [JsonProperty(PropertyName = "Url")]
        public string Url { get; set; }
        [JsonProperty(PropertyName = "ThumbUrl")]
        public string ThumbUrl { get; set; }
    }

    public class SlotCustomViewModel
    {
        [JsonProperty(PropertyName = "MediaId")]
        public string MediaId { get; set; }
        [JsonProperty(PropertyName = "ContentId")]
        public Guid ContentId { get; set; }
        [JsonProperty(PropertyName = "Url")]
        public string Url { get; set; }
    }

    public class SlotFixedViewModel
    {
        [JsonProperty(PropertyName = "Index")]
        public int Index { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "Url")]
        public string Url { get; set; }
        [JsonProperty(PropertyName = "ThumbUrl")]
        public string ThumbUrl { get; set; }
        [JsonProperty(PropertyName = "CategoryId")]
        public Guid CategoryId { get; set; }
        [JsonProperty(PropertyName = "SlotSatisticId")]
        public Guid SlotSatisticId { get; set; }
        [JsonProperty(PropertyName = "SlotType")]
        public SlotType SlotType { get; set; }
    }
}