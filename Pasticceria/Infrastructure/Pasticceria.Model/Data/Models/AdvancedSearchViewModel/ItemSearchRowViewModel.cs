﻿using Newtonsoft.Json;
using System;

namespace NFCConnect.Model.Data.Models.AdvancedSearchViewModel
{
    [Serializable]
    public class ItemSearchRowViewModel
    {
        [JsonProperty("MediaId")]
        public string MediaId { get; set; }
        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }
        [JsonProperty("CategoryId")]
        public Guid? CategoryId { get; set; }
        [JsonProperty("NTaps")]
        public long? NTaps { get; set; }
        [JsonProperty("NPlay")]
        public long? NPlay { get; set; }
        [JsonProperty("NViewTime")]
        public long? NViewTime { get; set; }
        [JsonProperty("Region")]
        public string Region { get; set; }
        [JsonProperty("City")]
        public string City { get; set; }
        [JsonProperty("DateFrom")]
        public DateTime? DateFrom { get; set; }
        [JsonProperty("DateTo")]
        public DateTime? DateTo { get; set; }
        [JsonProperty("ProductId")]
        public Guid ProductId { get; set; }
    }
}
