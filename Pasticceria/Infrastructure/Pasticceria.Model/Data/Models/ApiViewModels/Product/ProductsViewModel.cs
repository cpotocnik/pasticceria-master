﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Product
{
    [Serializable]
    public class ProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateOldestStatistic { get; set; }
        public DateTime DateNewestStatistic { get; set; }
    }
}
