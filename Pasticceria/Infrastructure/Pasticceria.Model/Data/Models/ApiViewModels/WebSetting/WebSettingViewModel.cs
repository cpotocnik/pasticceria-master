﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.WebSetting
{
    [Serializable]
    public class WebSettingViewModel
    {
        public string Language { get; set; }
        public string HeaderColor { get; set; }
        public string HeaderItemsColor { get; set; }
        public string DashboardColor { get; set; }
        public string LogoImageUrl { get; set; }
        public long MaxMediaSize { get; set; }
        public bool CustomTheme { get; set; }
    }
}
