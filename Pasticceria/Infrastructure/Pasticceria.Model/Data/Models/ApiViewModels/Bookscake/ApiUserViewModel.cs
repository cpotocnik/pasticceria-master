﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiUserViewModel
    {
        public string Email { get; set; }
        public DateTime? ActivatedAt { get; set; }
        public int PurchasedBooks { get; set; }
        public int CustomMedias { get; set; }
        public int GiftedBooks { get; set; }
        public int ReceivedGifts { get; set; }
        public string AvatarUrl { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
    }
}
