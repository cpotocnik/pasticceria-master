﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiDeviceViewModel
    {
        /// <summary>
        /// The device Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// The device identifier
        /// </summary>
        public string DeviceId { get; set; }
        /// <summary>
        /// The device name
        /// </summary>
        public string Name { get; set; }
    }
}
