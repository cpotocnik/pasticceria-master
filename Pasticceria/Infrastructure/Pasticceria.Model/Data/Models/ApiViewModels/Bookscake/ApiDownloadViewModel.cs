﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiDownloadViewModel
    {
        public string MediaId { get; set; }
        public string ChapterName { get; set; }
        public int ChapterIndex { get; set; }
        public long ChapterSize { get; set; }
        public Guid ItemDownloadId { get; set; }
    }
}
