﻿using System;
using System.Collections.Generic;
using System.Text;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiReportDownloadViewModel
    {
        public Guid ItemDownloadId { get; set; }
        public bool Found { get; set; }
        public bool Changed { get; set; }
        public DownloadStatus DownloadStatus { get; set; }

    }
}
