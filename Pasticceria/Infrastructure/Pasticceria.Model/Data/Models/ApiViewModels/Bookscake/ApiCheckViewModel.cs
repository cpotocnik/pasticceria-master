﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiCheckViewModel
    {
        public bool IsLocked { get; set; }
        public bool IsGift { get; set; }
        public bool IsAlreadyOwned { get; set; }
        public int DownloadsLeft { get; set; }
        public DateTime ExpiringDate { get; set; }
    }
}
