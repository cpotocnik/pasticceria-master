﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiUserCategoryViewModel
    {
        /// <summary>
        /// Id of the media
        /// </summary>
        public string MediaId { get; set; }

        /// <summary>
        /// Id of the category
        /// </summary>
        public Guid CategoryId { get; set; }

        /// <summary>
        /// Details of the book
        /// </summary>
        public ApiCategoryViewModel BookDetails { get; set; }

        /// <summary>
        /// Media link of the custom media if it has been uploaded
        /// </summary>
        public string CustomMediaLink { get; set; }

        /// <summary>
        /// To check if it's giftable
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// To check if books is already gifted
        /// </summary>
        public bool IsGiftable { get; set; }

        /// <summary>
        /// To check if the user is owner of the book
        /// </summary>
        public bool IsOwner { get; set; }

        /// <summary>
        /// Number of downloads remaining
        /// </summary>
        public int DownloadsLeft { get; set; }
    }
}
