﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiBookmarkViewModel
    {
        /// <summary>
        /// The user identifier
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// The media identifier
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// Chapter number
        /// </summary>
        public int Chapter { get; set; }
        /// <summary>
        /// The elapsed playing time
        /// </summary>
        public int ElapsedTime { get; set; }
    }
}
