﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class Chapter
    {
        /// <summary>
        /// Index order of the chapter
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Name of the chapter
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Media link of the chapter
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Duration of the chapter
        /// </summary>
        public int Duration { get; set; }
    }

    public class ApiCategoryViewModel
    {
        /// <summary>
        /// Book name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// cover of the book image
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Release date
        /// </summary>
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Plot
        /// </summary>
        public string Plot { get; set; }

        /// <summary>
        /// Author of the book
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// this is used with API "AddCustomVideo/{mediaId}/{contentId}" to upload a custom media
        /// </summary>
        public Guid ContentId { get; set; }

        /// <summary>
        /// Indexes and media links of the chapters
        /// </summary>
        public List<Chapter> Chapters { get; set; }

        /// <summary>
        /// Total duration of the book
        /// </summary>
        public int Duration { get; set; }
    }
}