﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Bookscake
{
    public class ApiNewsesViewModel
    {
        public List<ApiNewsViewModel> Newses { get; set; }
    }

    public class ApiNewsViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime StartsAt { get; set; }
        public DateTime EndsAt { get; set; }
    }
}
