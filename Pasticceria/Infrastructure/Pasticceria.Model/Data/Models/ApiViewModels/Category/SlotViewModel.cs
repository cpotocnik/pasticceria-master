﻿using Newtonsoft.Json;
using NFCConnect.Model.Data.Entities;
using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Category
{
    [Serializable]
    public class SlotViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "Index")]
        public int Index { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "SlotType")]
        public SlotType SlotType { get; set; }
        [JsonProperty(PropertyName = "Url")]
        public string Url { get; set; }
        [JsonProperty(PropertyName = "ThumbUrl")]
        public string ThumbUrl { get; set; }
        [JsonProperty(PropertyName = "MaxVideoLenght")]
        public int MaxVideoLenght { get; set; }
        [JsonProperty(PropertyName = "CategoryId")]
        public Guid CategoryId { get; set; }
        [JsonProperty(PropertyName = "ValidFrom")]
        public DateTime ValidFrom { get; set; }
        [JsonProperty(PropertyName = "ValidTo")]
        public DateTime ValidTo { get; set; }
    }
}
