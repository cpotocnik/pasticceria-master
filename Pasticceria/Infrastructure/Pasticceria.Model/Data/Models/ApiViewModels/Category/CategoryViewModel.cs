﻿using Newtonsoft.Json;
using NFCConnect.Model.Data.Entities;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Category
{
    [Serializable]
    public class CategoryViewModel
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "ValidFrom")]
        public DateTime ValidFrom { get; set; }
        [JsonProperty(PropertyName = "ValidTo")]
        public DateTime ValidTo { get; set; }
        [JsonProperty(PropertyName = "NItems")]
        public long NItems { get; set; }
        [JsonProperty(PropertyName = "NTaps")]
        public long NTaps { get; set; }
        [JsonProperty(PropertyName = "ProductId")]
        public Guid ProductId { get; set; }
        [JsonProperty(PropertyName = "ParentId")]
        public Guid? ParentId { get; set; }
        [JsonProperty(PropertyName = "ImageUrl")]
        public string ImageUrl { get; set; }
        [JsonProperty(PropertyName = "ReleaseDate")]
        public DateTime? ReleaseDate { get; set; }
        [JsonProperty(PropertyName = "Plot")]
        public string Plot { get; set; }
        [JsonProperty(PropertyName = "Author")]
        public string Author { get; set; }
        [JsonProperty(PropertyName = "Speaker")]
        public string Speaker { get; set; }
        [JsonProperty(PropertyName = "Slots")]
        public ICollection<Slot> Slots { get; set; }
        [JsonProperty(PropertyName = "FadingTimeout")]
        public int FadingTimeout { get; set; }
    }
}
