﻿using Newtonsoft.Json;
using NFCConnect.Model.Data.Models.ItemViewModels;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Item
{
    [Serializable]
    public class ItemTableViewModel
    {
        [JsonProperty(PropertyName = "RecordsTotal")]
        public int RecordsTotal { get; set; }
        [JsonProperty(PropertyName = "ItemRowViewModels")]
        public List<ItemRowViewModel> ItemRowViewModels { get; set; }
    }
}
