﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Account
{
    public class UserViewModel
    {
        public string Email { get; set; }
        public string UserId { get; set; }
        public DateTime? ActivatedAt { get; set; }
        public string AccessToken { get; set; }
        public bool IsAdmin { get; set; }
    }
}
