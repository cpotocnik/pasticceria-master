﻿using NFCConnect.Model.Data.Entities;
using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Report
{
    [Serializable]
    public class ReportViewModel
    {
        public string Url { get; set; }
        public ReportFrequency ReportFrequency { get; set; }
        public List<MailViewModel> MailViewModels { get; set; }
    }

    [Serializable]
    public class MailViewModel
    {
        public string Email { get; set; }
    }
}
