﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class TimeTapsViewModel
    {
        public int MaxTaps { get; set; }
        public List<CategoryTapsViewModel> CategoryTaps { get; set; }
    }

    [Serializable]
    public class CategoryTapsViewModel
    {
        /// <summary>
        /// Category Name
        /// </summary>
        public string CategoryName { get; set; }
        /// <summary>
        /// List of 13 integers with average from 0 to 24, every 2 hours.
        /// </summary>
        public List<HourTaps> Taps { get; set; }
    }

    [Serializable]
    public class HourTaps
    {
        public int Hour { get; set; }
        public int AverageTaps { get; set; }
    }
}
