﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class OsUsageViewModel
    {
        /// <summary>
        /// Os Name
        /// </summary>
        public string Os { get; set; }
        /// <summary>
        /// Percentage of usage from this Os
        /// </summary>
        public double Percentage { get; set; }
    }
}
