﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    public class ItemStatisticPanelViewModel
    {
        public string MediaId { get; set; }
        public string CustomerId { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Ip { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Os { get; set; }
        public string Language { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string PostalCode { get; set; }
        public string Organisation { get; set; }
        public bool Geolocalized { get; set; }
        public int TotalSlotStatistics { get; set; }
        public List<SlotStatisticRowViewModel> SlotStatisticRowViewModels { get; set; }
    }
}
