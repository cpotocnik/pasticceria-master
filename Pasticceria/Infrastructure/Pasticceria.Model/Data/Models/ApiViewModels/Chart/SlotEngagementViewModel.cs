﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class SlotEngagementViewModel
    {
        public int MaxNTaps { get; set; }
        public List<SlotNTapsViewModel> SlotNTaps { get; set; }
    }

    [Serializable]
    public class SlotNTapsViewModel
    {
        public int Index { get; set; }
        public string SlotName { get; set; }
        public int NTaps { get; set; }
    }
}
