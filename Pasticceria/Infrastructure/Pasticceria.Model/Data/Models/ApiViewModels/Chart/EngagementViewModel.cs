﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class EngagementViewModel
    {
        public int MaxMediaLength { get; set; }
        public double MaxPercentage { get; set; }
        public List<CategoryEngagement> CategoryEngagements { get; set; }
    }

    [Serializable]
    public class CategoryEngagement
    {
        public string CategoryName { get; set; }
        public List<RangeEngagement> RangeEngagements { get; set; }
    }

    [Serializable]
    public class RangeEngagement
    {
        public int SecondsRange { get; set; }
        public double Percentage { get; set; }
    }
}
