﻿using System;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class ChartRequestViewModel
    {
        /// <summary>
        /// Product Id
        /// </summary>
        public Guid ProdId { get; set; }
        /// <summary>
        /// Category Id
        /// </summary>
        public Guid CatId { get; set; }
        /// <summary>
        /// MediaId
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// ItemStatisticId
        /// </summary>
        public Guid ItemStatisticId { get; set; }
        /// <summary>
        /// Start DateTime (optional)
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End DateTime (optional)
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}
