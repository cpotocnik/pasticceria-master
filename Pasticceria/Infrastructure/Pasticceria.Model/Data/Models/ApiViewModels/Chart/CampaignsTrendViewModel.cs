﻿using System;
using System.Collections.Generic;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    [Serializable]
    public class CampaignsTrendViewModel
    {
        public int MaxTaps { get; set; }
        public int NRanges { get; set; }
        public List<CategoryTrend> CampaignTrends { get; set; }
    }

    [Serializable]
    public class CategoryTrend
    {
        public string CategoryName { get; set; }
        public List<RangeTrend> RangeTrends { get; set; }
    }

    [Serializable]
    public class RangeTrend
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Taps { get; set; }
    }
}
