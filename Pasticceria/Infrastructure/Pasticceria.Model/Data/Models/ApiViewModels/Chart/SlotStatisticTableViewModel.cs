﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFCConnect.Model.Data.Models.ApiViewModels.Chart
{
    public class SlotStatisticTableViewModel
    {
        public int RecordsTotal { get; set; }
        public List<SlotStatisticRowViewModel> SlotStatisticRowViewModels { get; set; }
    }

    public class SlotStatisticRowViewModel
    {
        public string MediaId { get; set; }
        public Guid StatisticId { get; set; }
        public string SlotName { get; set; }
        public int NTaps { get; set; }
        public int NViewTime { get; set; }
        public int TotalMediaLength { get; set; }
        public DateTime LocalDateTime { get; set; }
    }
}
