﻿namespace NFCConnect.Data.Sessions
{
    public static class SessionKeys
    {
        public const string ProductsForDropDown = "ProductsForDropDown";
        public const string ProductId = "ProductId";
    }
}