﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface IApiResourcesRepository
    {
        IQueryable<APIResource> GetAll();
        IQueryable<APIResource> GetAPIResource(int Id);

    }
    public class ApiResourcesRepository : IApiResourcesRepository
    {
        protected readonly DbContext _context;
        public ApiResourcesRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<APIResource> GetAll()
        {
            return _context.Set<APIResource>();
        }

        public IQueryable<APIResource> GetAPIResource(int Id)
        {
            return _context.Set<APIResource>().Where(c => c.Id == Id);
        }

    }

}