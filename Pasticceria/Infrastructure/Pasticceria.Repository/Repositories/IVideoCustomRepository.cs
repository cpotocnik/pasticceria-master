﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Repository.Repositories.Base;

namespace NFCConnect.Repository.Repositories
{
    public interface IVideoCustomRepository : IGenericCounterRepository<VideoCustom>
    {

    }

    public class VideoCustomRepository : GenericCounterRepository<VideoCustom>, IVideoCustomRepository
    {
        public VideoCustomRepository(DbContext context) : base(context)
        {
        }
    }
}