﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Repository.Repositories
{
    public interface IUserCategoryRepository : IRepository<UserCategory>
    {

    }

    public class UserCategoryRepository : RepositoryEF<UserCategory>, IUserCategoryRepository
    {
        public UserCategoryRepository(DbContext context) : base(context)
        {
        }
    }
}
