﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities.Base;
using NFCConnect.Model.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NFCConnect.Repository.Repositories.Base
{
    public interface IGenericCounterRepository<T> : IRepositoryValidity<T> where T : EntityCounter
    {
        ICollection<DropDownViewModel> GetForDropDown(bool withNull = false);
    }

    public class GenericCounterRepository<T> : RepositoryEFValidity<T>, IGenericCounterRepository<T> where T : EntityCounter
    {
        public GenericCounterRepository(DbContext context) : base(context)
        {
        }

        public ICollection<DropDownViewModel> GetForDropDown(bool withNull = false)
        {
            var result = ValidAndActive().Select(x => new DropDownViewModel { Name = x.Name, Value = x.Id }).OrderBy(x => x.Name).ToList();
            if (withNull)
            {
                result.Insert(0, new DropDownViewModel { Name = "Nessuna", Value = Guid.Empty });
            }

            return result;
        }
    }
}
