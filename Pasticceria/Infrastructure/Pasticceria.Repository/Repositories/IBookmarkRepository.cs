﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Repository.Repositories
{
    public interface IBookmarkRepository : IRepository<Bookmark>
    {
    }

    public class BookmarkRepository : RepositoryEF<Bookmark>, IBookmarkRepository
    {
        public BookmarkRepository(DbContext context) : base(context)
        {
        }
    }
}
