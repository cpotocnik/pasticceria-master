﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Repository.Repositories
{
    public interface IWebSettingRepository : IRepository<WebSetting>
    {
    }

    public class WebSettingRepository : RepositoryEF<WebSetting>, IWebSettingRepository
    {
        public WebSettingRepository(DbContext context) : base(context)
        {
        }
    }
}
