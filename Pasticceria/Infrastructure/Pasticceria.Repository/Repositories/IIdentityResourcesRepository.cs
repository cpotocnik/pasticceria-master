﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface IIdentityResourcesRepository
    {
        IQueryable<IdentityResource> GetAll();
        IQueryable<IdentityResource> GetIdentityResources(int id);
    }

    public class IdentityResourcesRepository : IIdentityResourcesRepository
    {
        protected readonly DbContext _context;
        public IdentityResourcesRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<IdentityResource> GetAll()
        {
            return _context.Set<IdentityResource>();
        }

        public IQueryable<IdentityResource> GetIdentityResources(int id)
        {
            return _context.Set<IdentityResource>().Where(x => x.Id == id);
        }

    }
}