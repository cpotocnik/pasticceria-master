﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Repository.Repositories
{
    public interface IReportSettingRepository : IRepository<ReportSetting>
    {
    }

    public class ReportSettingRepository : RepositoryEF<ReportSetting>, IReportSettingRepository
    {
        public ReportSettingRepository(DbContext context) : base(context)
        {
        }
    }
}
