﻿using System;
using System.Collections.Generic;
using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IItemDownloadRepository : IRepository<ItemDownload>
    {
        ItemDownload GetById(Guid itemDownloadId);
        IQueryable<ItemDownload> GetManyByIds(List<Guid> itemDownloadIds);
        int GetItemNDownloadsByItemId(Guid itemId);
    }

    public class ItemDownloadRepository : RepositoryEF<ItemDownload>, IItemDownloadRepository
    {
        protected readonly DbContext _context;
        public ItemDownloadRepository(DbContext context) : base(context)
        {
            _context = context;
        }

        public ItemDownload GetById(Guid itemDownloadId)
        {
            return _context.Set<ItemDownload>().FirstOrDefault(x => x.Id == itemDownloadId);
        }

        public IQueryable<ItemDownload> GetManyByIds(List<Guid> itemDownloadIds)
        {
            return _context.Set<ItemDownload>().Where(x => itemDownloadIds.Contains(x.Id));
        }

        public int GetItemNDownloadsByItemId(Guid itemId)
        {
            var downloads = _context.Set<ItemDownload>().Where(x => x.ItemId == itemId);
            return downloads.Any() ? downloads.Max(x => x.DownloadNumber) : 0;
        }
    }
}
