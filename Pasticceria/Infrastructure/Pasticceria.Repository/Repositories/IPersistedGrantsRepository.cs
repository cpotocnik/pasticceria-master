﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface IPersistedGrantsRepository
    {
        IQueryable<PersistedGrant> GetAll();
        IQueryable<PersistedGrant> GetPersistedGrant(string key);
    }

    public class PersistedGrantsRepository : IPersistedGrantsRepository
    {
        protected readonly DbContext _context;
        public PersistedGrantsRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<PersistedGrant> GetAll()
        {
            return _context.Set<PersistedGrant>();
        }

        public IQueryable<PersistedGrant> GetPersistedGrant(string key)
        {
            return _context.Set<PersistedGrant>().Where(x => x.Key == key);
        }

    }
}