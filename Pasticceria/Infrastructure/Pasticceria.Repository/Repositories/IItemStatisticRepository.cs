﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IItemStatisticRepository : IRepository<ItemStatistic>
    {
        Task<ItemStatistic> GetWithSlotStat(Guid id);
    }

    public class ItemStatisticRepository : RepositoryEF<ItemStatistic>, IItemStatisticRepository
    {
        public ItemStatisticRepository(DbContext context) : base(context)
        {
        }

        public async Task<ItemStatistic> GetWithSlotStat(Guid id)
        {
            return await GetAllIncluding(x => x.SlotStatistics).FirstOrDefaultAsync(x => x.Id.Equals(id));
        }
    }
}