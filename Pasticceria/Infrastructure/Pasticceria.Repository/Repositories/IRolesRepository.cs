﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IRolesRepository
    {
        IQueryable<Role> GetAll();
        IQueryable<Role> GetRole(string id);
        Task CreateRole(string name);
        Task DeleteRole(string name);
    }

    public class RolesRepository : IRolesRepository
    {
        public DbContext _context;
        public RoleManager<IdentityRole> _rolesManager;
        public RolesRepository(DbContext context, RoleManager<IdentityRole> rolesManager)
        {
            _context = context;
            _rolesManager = rolesManager;
        }

        public IQueryable<Role> GetAll()
        {
            return _context.Set<Role>();
        }

        public IQueryable<Role> GetRole(string id)
        {
            return _context.Set<Role>().Where(x => x.Id == id);
        }

        public async Task CreateRole(string name)
        {
            try
            {
                var newRole = new IdentityRole() { Name = name };
                if (!_rolesManager.RoleExistsAsync(name).Result)
                {
                    await _rolesManager.CreateAsync(newRole);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task DeleteRole(string name)
        {
            var rol = _rolesManager.FindByNameAsync(name).Result;
            await _rolesManager.DeleteAsync(rol);
        }
    }
}