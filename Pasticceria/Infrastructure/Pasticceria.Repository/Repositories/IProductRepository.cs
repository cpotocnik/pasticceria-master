﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Repository.Repositories.Base;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface IProductRepository : IGenericCounterRepository<Product>
    {
        Product GetFirst();
    }

    public class ProductRepository : GenericCounterRepository<Product>, IProductRepository
    {
        public ProductRepository(DbContext context) : base(context)
        {
        }

        public Product GetFirst()
        {
            return ValidAndActive().OrderBy(x => x.Name).FirstOrDefault();
        }
    }
}