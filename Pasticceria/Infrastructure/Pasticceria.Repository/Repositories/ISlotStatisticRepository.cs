﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository
{
    public interface ISlotStatisticRepository : IRepository<SlotStatistic>
    {
        Task<SlotStatistic> GetById(Guid slotStatisticId);
        Task<SlotStatistic> GetBySlotIdStatId(Guid slotId, Guid itemStatisticId);
    }

    public class SlotStatisticRepository : RepositoryEF<SlotStatistic>, ISlotStatisticRepository
    {
        protected readonly DbContext _context;
        public SlotStatisticRepository(DbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<SlotStatistic> GetById(Guid slotStatisticId)
        {
            return await _context.Set<SlotStatistic>().Where(x => x.Id == slotStatisticId).FirstOrDefaultAsync();
        }

        public async Task<SlotStatistic> GetBySlotIdStatId(Guid slotId, Guid itemStatisticId)
        {
            return await _context.Set<SlotStatistic>().Where(x => x.SlotId == slotId && x.ItemStatisticId == itemStatisticId).FirstOrDefaultAsync();
        }
    }
}
