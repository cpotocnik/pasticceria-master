﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface ITranslationsRepository
    {
        Task<List<Translation>> GetTranslations(LanguageCode languageCode);
        Task UpdateTranslationsRange(List<Translation> translations);
        Task UpdateSingleTranslation(Translation translation);
        List<LanguageCode> GetLanguageCodes();
    }

    public class TranslationsRepository : ITranslationsRepository
    {
        protected readonly DbContext _context;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TranslationsRepository(DbContext context)
        {
            _context = context;
        }

        public List<LanguageCode> GetLanguageCodes()
        {
            try
            {
                return _context.Set<Translation>().Select(s => s.LanguageCode).Distinct().ToList();
            }
            catch (Exception ex) { return new List<LanguageCode>(); }
        }

        public async Task<List<Translation>> GetTranslations(LanguageCode languageCode)
        {
            if (languageCode == null)
            {
                return null;
            }
            return _context.Set<Translation>().Where(p => p.LanguageCode == languageCode).ToList();
        }

        public async Task UpdateTranslationsRange(List<Translation> translations)
        {
            _context.Set<Translation>().UpdateRange(translations);
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex) { }
        }

        public async Task UpdateSingleTranslation(Translation translation)
        {
            _context.Set<Translation>().Update(translation);
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex) { }
        }
    }
}
