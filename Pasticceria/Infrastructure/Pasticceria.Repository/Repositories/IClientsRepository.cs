﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface IClientsRepository
    {
        IQueryable<Client> GetAll();
        IQueryable<Client> GetClient(int Id);
    }

    public class ClientsRepository : IClientsRepository
    {
        protected readonly DbContext _context;
        public ClientsRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<Client> GetAll()
        {
            return _context.Set<Client>();
        }

        public IQueryable<Client> GetClient(int Id)
        {
            return _context.Set<Client>().Where(c => c.Id == Id);
        }

    }
}