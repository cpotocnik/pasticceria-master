﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Repository.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface ISlotRepository : IGenericCounterRepository<Slot>
    {
        Slot GetSlotById(Guid slotId);
        IQueryable<Slot> GetSlotByCategoryId(Guid? categoryId);
        List<Slot> GetSlotByCategoryIds(List<Guid> categoryIds);
        List<Slot> GetSlots(List<Guid> ids);
    }

    public class SlotRepository : GenericCounterRepository<Slot>, ISlotRepository
    {
        public SlotRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Slot> GetSlotByCategoryId(Guid? categoryId)
        {
            return !categoryId.HasValue ? null : Valid().Where(x => x.CategoryId == categoryId);
        }

        public List<Slot> GetSlotByCategoryIds(List<Guid> categoryIds)
        {
            return Valid().Where(x => categoryIds.Contains(x.CategoryId)).ToList();
        }

        public Slot GetSlotById(Guid slotId)
        {
            return Valid().FirstOrDefault(x => x.Id.Equals(slotId));
        }

        public List<Slot> GetSlots(List<Guid> ids)
        {
            return ValidAndActive().Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}