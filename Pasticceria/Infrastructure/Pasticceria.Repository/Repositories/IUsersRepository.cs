﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Models;
using NFCConnect.Models.AccountViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IUsersRepository
    {
        IQueryable<ApplicationUser> GetAll();
        IQueryable<ApplicationUser> GetUser(string Id);
        Task<ApplicationUser> CreateUserAsync(RegisterViewModel regUser);
        Task Delete(string id);
        Task ChangePassword(string Email, string oldPassword, string password, string confirmPassword);
        Task AddPhoneNumber(string Id, string phone);
    }

    public class UsersRepository : IUsersRepository
    {
        public UserManager<ApplicationUser> _userManager;
        protected readonly DbContext _context;
        public UsersRepository(
            DbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IQueryable<ApplicationUser> GetAll()
        {
            return _userManager.Users;
        }

        public IQueryable<ApplicationUser> GetUser(string Id)
        {
            return _context.Set<ApplicationUser>().Where(x => x.Id == Id);
        }

        public async Task<ApplicationUser> CreateUserAsync(RegisterViewModel regUser)
        {
            var user = new ApplicationUser { UserName = regUser.Email, Email = regUser.Email, RegistrationDate = DateTime.Now };
            var result = await _userManager.CreateAsync(user, regUser.Password);
            if (result.Succeeded)
            {
                var usr = _userManager.FindByEmailAsync(regUser.Email).Result;
                var token = _userManager.GenerateEmailConfirmationTokenAsync(usr).Result;
                await _userManager.ConfirmEmailAsync(usr, token);
                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task Delete(string id)
        {
            await _userManager.DeleteAsync(_userManager.FindByIdAsync(id).Result);
        }

        public async Task ChangePassword(string Email, string oldPassword, string password, string confirmPassword)
        {
            if (password.Equals(confirmPassword))
            {
                var result = await _userManager.ChangePasswordAsync(_userManager.FindByEmailAsync(Email).Result, oldPassword, password);
            }
        }

        public async Task AddPhoneNumber(string Id, string phone)
        {
            await _userManager.SetPhoneNumberAsync(_userManager.FindByIdAsync(Id).Result, phone);
        }
    }
}