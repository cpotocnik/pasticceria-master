﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Model.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IUserRolesRepository
    {
        IQueryable<UserRole> GetAll();
        IQueryable<UserRole> GetUserRole(string userId);
        Task<bool> IsInRoleAsync(ApplicationUser user, string role);
        Task<IdentityResult> AssignRoles(string Id, string role);
        Task<IdentityResult> RemoveRoles(string Id, string role);
    }

    public class UserRolesRepository : IUserRolesRepository
    {
        protected readonly DbContext _context;
        public UserManager<ApplicationUser> _userManager;
        public UserRolesRepository(
            DbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IQueryable<UserRole> GetAll()
        {
            return _context.Set<UserRole>();
        }

        public IQueryable<UserRole> GetUserRole(string userId)
        {
            return _context.Set<UserRole>().Where(x => x.UserId == userId);
        }

        public async Task<IdentityResult> AssignRoles(string Id, string role)
        {
            var user = _userManager.FindByIdAsync(Id).Result;
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> RemoveRoles(string Id, string role)
        {
            var user = _userManager.FindByIdAsync(Id).Result;
            return await _userManager.RemoveFromRoleAsync(user, role);
        }

        public async Task<bool> IsInRoleAsync(ApplicationUser user, string role)
        {
            return await _userManager.IsInRoleAsync(user, role);
        }
    }
}