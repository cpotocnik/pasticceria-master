﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Model.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IOtpRepository : IRepository<OTP>
    {
        Task InsertData(List<OTP> otpList);
        OTP GetOtp(string otp);
        Task<List<OTP>> AddRangeAsyn(List<OTP> otps, DbContext context);
    }
    public class OtpRepository : RepositoryEF<OTP>, IOtpRepository
    {
        protected readonly DbContext _context;        
        public OtpRepository(DbContext context) : base(context)
        {
            _context = context;            
        }

        public async Task InsertData(List<OTP> otpList)
        {
            await _context.Set<OTP>().AddRangeAsync(otpList);
            await _context.SaveChangesAsync();
        }

        public OTP GetOtp(string otp)
        {
            return _context.Set<OTP>().SingleOrDefault(x => x.Otp == otp);
        }       

        public bool ValidateDb()
        {
            return (_context.Set<OTP>().Any()) ? true : false;
        }

        public async Task<List<OTP>> AddRangeAsyn(List<OTP> otps, DbContext context)
        {
            context.ChangeTracker.AutoDetectChangesEnabled = false;
            await context.Set<OTP>().AddRangeAsync(otps);
            context.ChangeTracker.DetectChanges();
            await context.SaveChangesAsync();
            context?.Dispose();
            return otps;
        }
    }
}
