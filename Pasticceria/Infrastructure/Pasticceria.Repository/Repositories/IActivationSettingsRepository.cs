﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IActivationSettingsRepository
    {
        bool IsActive();
        bool IsNew();
        Task<IActionResult> AddConfiguration(int duration);
        Task<IActionResult> UpdateConfiguration(int duration);
        Task Disable();
        DateTime GetExpiration();
        DateTime GetActivation();
        ActivationSettings GetData();
    }

    public class ActivationSettingsRepository : IActivationSettingsRepository
    {
        protected readonly DbContext _context;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public ActivationSettingsRepository(DbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsActive()
        {
            var dateExpiration = GetExpiration();
            if (dateExpiration == null || dateExpiration > DateTime.Now || dateExpiration == DateTime.MaxValue)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        public async Task<IActionResult> AddConfiguration(int duration)
        {
            try
            {
                var settings = new ActivationSettings()
                {
                    Duration = duration,
                    ActivationDate = DateTime.Now
                };
                _context.Add(settings);
                _context.SaveChanges();
                return new OkObjectResult(true);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        public async Task<IActionResult> UpdateConfiguration(int duration)
        {
            try
            {
                var settings = new ActivationSettings()
                {
                    Duration = duration,
                    ActivationDate = DateTime.Now
                };
                var actual = _context.Set<ActivationSettings>().First();
                if (actual != null && actual.Duration != duration)
                {
                    actual.Duration = duration;
                }
                _context.SaveChanges();
                return new OkObjectResult(true);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task Disable()
        {
            try
            {
                var currentActivationSettings = _context.Set<ActivationSettings>().FirstAsync().Result;
                if (currentActivationSettings != null)
                {
                    _context.Remove(currentActivationSettings);
                }
                _context.SaveChanges();
            }
            catch (Exception ex) { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime GetExpiration()
        {
            var currentActivationSettings = _context.Set<ActivationSettings>().Any() ? _context.Set<ActivationSettings>().FirstAsync().Result : new ActivationSettings(){ Duration = 0, ActivationDate = DateTime.MaxValue}; 
            return currentActivationSettings.ActivationDate.AddDays(currentActivationSettings.Duration);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime GetActivation()
        {
            return _context.Set<ActivationSettings>().Any() ? _context.Set<ActivationSettings>().FirstAsync().Result.ActivationDate : DateTime.Now;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return (_context.Set<ActivationSettings>().Any()) ? false : true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActivationSettings GetData()
        {
            var currentActivationSettings = new ActivationSettings();
            try
            {
                currentActivationSettings = _context.Set<ActivationSettings>().Any() ? _context.Set<ActivationSettings>().FirstAsync().Result :  new ActivationSettings(){  Duration = -1};
            }
            catch (Exception ex)
            {
            }
            return currentActivationSettings;
        }
    }
}
