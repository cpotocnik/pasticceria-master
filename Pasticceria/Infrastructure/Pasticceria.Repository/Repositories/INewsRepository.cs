﻿using AGJ.Data.Repository;
using AGJ.Data.Repository.EF;
using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;

namespace NFCConnect.Repository.Repositories
{
    public interface INewsRepository : IRepository<News>
    {

    }

    public class NewsRepository : RepositoryEF<News>, INewsRepository
    {
        public NewsRepository(DbContext context) : base(context)
        {
        }
    }
}
