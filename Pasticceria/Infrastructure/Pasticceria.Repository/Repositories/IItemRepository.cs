﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Repository.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCConnect.Repository.Repositories
{
    public interface IItemRepository : IGenericCounterRepository<Item>
    {
        IQueryable<Item> GetTable(Guid productId);
        Item GetByMediaId(string mediaId);
        Item GetByCustomerId(string customerId);
        Item GetByMediaIdWithDownloads(string mediaId);
        Item GetByCustomerIdWithDownloads(string customerId);
        List<Item> GetByMediaIds(List<string> mediaIds);
        Task<List<Item>> AddRangeAsyn(List<Item> items, DbContext context);

    }

    public class ItemRepository : GenericCounterRepository<Item>, IItemRepository
    {
        public ItemRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Item> GetTable(Guid productId)
        {
            return ValidAndActive().Where(x => x.ProductId == productId);
        }

        public IQueryable<Item> GetByCategoryId(Guid categoryId)
        {
            return ValidAndActive().Where(x => x.CategoryId == categoryId);
        }

        public Item GetByMediaId(string mediaId)
        {
            return ValidAndActive().Where(x => x.MediaId == mediaId).SingleOrDefault();
        }

        public Item GetByCustomerId(string customerId)
        {
            return ValidAndActive().Where(x => x.CustomerId == customerId).SingleOrDefault();
        }

        public Item GetByMediaIdWithDownloads(string mediaId)
        {
            return ValidAndActive().Include(x => x.ItemDownloads).Where(x => x.MediaId == mediaId).SingleOrDefault();
        }

        public Item GetByCustomerIdWithDownloads(string customerId)
        {
            return ValidAndActive().Include(x => x.ItemDownloads).Where(x => x.CustomerId == customerId).SingleOrDefault();
        }

        public List<Item> GetByMediaIds(List<string> mediaIds)
        {
            return ValidAndActive().Where(x => mediaIds.Contains(x.MediaId)).ToList();
        }

        public async Task<List<Item>> AddRangeAsyn(List<Item> items, DbContext context)
        {
            context.ChangeTracker.AutoDetectChangesEnabled = false;
            await context.Set<Item>().AddRangeAsync(items);
            context.ChangeTracker.DetectChanges();
            await context.SaveChangesAsync();
            context?.Dispose();
            return items;
        }
    }
}