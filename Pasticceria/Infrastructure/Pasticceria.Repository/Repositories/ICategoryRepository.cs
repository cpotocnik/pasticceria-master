﻿using Microsoft.EntityFrameworkCore;
using NFCConnect.Model.Data.Entities;
using NFCConnect.Model.Data.Models;
using NFCConnect.Repository.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NFCConnect.Repository.Repositories
{
    public interface ICategoryRepository : IGenericCounterRepository<Category>
    {
        Category GetCategoryWithSubcategory(Guid categoryId);
        List<Category> GetByProductId(Guid productId);
        List<Category> GetAllByProductId(Guid productId);
        Category GetWithSlots(Guid categoryId);
        List<DropDownViewModel> GetForDropDownByProducts(Guid productId, bool withNull = false);
        List<Category> GetCategories(List<Guid> ids);
        Category GetCategory(Guid id);
        List<Guid> GetCategoryIds(Guid categoryId);
        List<Guid> GetSubCategoryIds(Guid categoryId);
        List<Guid> GetIdsByProductId(Guid productId);
    }

    public class CategoryRepository : GenericCounterRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }

        public Category GetCategoryWithSubcategory(Guid categoryId)
        {
            throw new NotImplementedException();
        }

        public List<Category> GetByProductId(Guid productId)
        {
            var result = ValidAndActive().Where(x => x.ProductId == productId).Where(x => x.ParentId == null)
                .Include(x => x.SubCategories)
                .ThenInclude(x => x.SubCategories)
                .ThenInclude(x => x.SubCategories)
                .ToList();

            return result;
        }

        public List<Category> GetAllByProductId(Guid productId)
        {
            var result = GetAll().Where(x => x.ProductId == productId).ToList();

            return result;
        }

        public Category GetWithSlots(Guid categoryId)
        {
            return GetAllIncluding(x => x.Slots).SingleOrDefault(x => x.Id == categoryId);
        }

        public List<DropDownViewModel> GetForDropDownByProducts(Guid productId, bool withNull = false)
        {
            var result = ValidAndActive().Where(x => x.ProductId == productId).Select(x => new DropDownViewModel { Name = x.Name, Value = x.Id }).OrderBy(x => x.Name).ToList();
            if (withNull)
            {
                result.Insert(0, new DropDownViewModel { Name = "Nessuna", Value = Guid.Empty });
            }

            return result;
        }

        public List<Category> GetCategories(List<Guid> ids)
        {
            return ValidAndActive().Where(x => ids.Contains(x.Id)).ToList();
        }

        public Category GetCategory(Guid id)
        {
            var category = ValidAndActive().FirstOrDefault(x => id.Equals(x.Id));
            return category;
        }


        public List<Guid> GetCategoryIds(Guid categoryId)
        {
            var ids = new List<Guid> { categoryId };

            var category = Get(categoryId);
            while (category.ParentId != null)
            {
                category = Get(category.ParentId.Value);
                ids.Insert(0, category.Id);
            }

            return ids;
        }

        public List<Guid> GetSubCategoryIds(Guid categoryId)
        {
            var result = new List<Guid>();
            var ids = GetAll().Where(x => x.ParentId == categoryId).Select(x => x.Id).ToList();
            result.AddRange(ids);
            foreach (var guid in ids)
            {
                result.AddRange(GetSubCategoryIdsRecursive(guid));
            }

            return result;
        }

        public List<Guid> GetIdsByProductId(Guid productId)
        {
            var ids = GetAll().Where(x => x.ProductId == productId).Select(x => x.Id).ToList();

            return ids;
        }

        private List<Guid> GetSubCategoryIdsRecursive(Guid categoryId)
        {
            var result = new List<Guid>();
            var ids = GetAll().Where(x => x.ParentId == categoryId).Select(x => x.Id).ToList();
            result.AddRange(ids);
            foreach (var guid in ids)
            {
                result.AddRange(GetSubCategoryIdsRecursive(guid));
            }
            return result;
        }
    }
}