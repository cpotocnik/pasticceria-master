using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace Pasticceria.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var env = builderContext.HostingEnvironment;
                    config

                    .AddJsonFile($"Settings/appsettings.json", optional: false, reloadOnChange: false);
                })
                .UseIISIntegration()
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment();
                })
                .UseStartup<Startup>()
                .Build();
        }
        private const string ConfigurationHost = "ASPNETCORE_ENVIRONMENT";
        private static string LocalIdentifier => !string.IsNullOrWhiteSpace(Environment.MachineName) ? Environment.MachineName : (!string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable(ConfigurationHost))) ? Environment.GetEnvironmentVariable(ConfigurationHost) : string.Empty;
    }
}


